﻿using asp.netSiteFw.DataContext;
using asp.netSiteFw.Models;
using asp.netSiteFw.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace asp.netSiteFw.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult LogOut()
        {
            var res = HttpContext.Response.Cookies["username"];
            res.Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Index");
        }

        public ActionResult DetailProduct(Guid id)
        {
            using (var context = new ShopContext())
            {
                var product = context.Products.Where(p => p.Id == id).FirstOrDefault();
                return View(product);
            }
        }

        public ActionResult DeleteProduct(Guid id)
        {
            using (var context = new ShopContext())
            {
                var product = context.Products.Where(p => p.Id == id).FirstOrDefault();
                return View(product);
            }
        }

        [HttpPost]
        public ActionResult DeleteProduct(Product product)
        {
            using (var context = new ShopContext())
            {
                context.Entry(product).State = EntityState.Deleted;
                context.SaveChanges();
                return RedirectToAction("ProductsTable");
            }
        }

        public ActionResult EditProduct(Guid id)
        {
            using (var context = new ShopContext())
            {
                var product = context.Products.Where(p => p.Id == id).FirstOrDefault();
                return View(product);
            }
        }

        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            using (var context = new ShopContext())
            {
                //var productEdited = context.Products.FirstOrDefault(p => p.Id == product.Id);
                //context.Entry(productEdited).CurrentValues.SetValues(product);
                //productEdited = product;
                context.Entry(product).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("ProductsTable");
            }
        }

        public ActionResult ProductsTable(string searchProduct = "", int page = 1)
        {
            using (var context = new ShopContext())
            {
                var productList = context.Products.ToList();
                if (searchProduct != "")
                {
                    productList = context.Products.Where(item => item.Name.Contains(searchProduct)).ToList();
                }
                int pageSize = 3;
                IEnumerable<Product> productsPerPages = productList.Skip((page - 1) * pageSize).Take(pageSize);
                PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = productList.Count };
                IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Products = productsPerPages };
                return View(ivm);

            }
        }

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(User user)
        {
            using (var context = new ShopContext())
            {
                if (!context.Users.Any(u => u.Login == user.Login))
                {
                    return RedirectToAction("Registration");
                }
                else
                {
                    var contextUser = context.Users.Where(u => u.Login == user.Login).FirstOrDefault();

                    if (contextUser.Password == user.Password)
                    {
                        var cookie = new HttpCookie("username", user.Login);
                        HttpContext.Response.Cookies.Add(cookie);
                        var req = HttpContext.Request.Cookies["username"];
                        req.Expires = DateTime.Now.AddMinutes(20);
                        return RedirectToAction("Index");
                    }

                    ModelState.AddModelError("Password", "Wrong password");
                    return View(user);
                }
            }

        }

        public ActionResult UsersTable()
        {
            using (var context = new ShopContext())
            {
                var userList = context.Users.ToList();
                return View(userList);
            }
        }

        public ActionResult UserExists()
        {
            using (var context = new ShopContext())
            {
                var userList = context.Users.ToList();
                return View(userList);
            }
        }

        public ActionResult Registration()
        {
            ViewBag.RolesInput = new SelectList(new List<string>
            {
                "admin", "user", "manager"
            });
            return View();
        }

        [HttpPost]
        public ActionResult Registration(User user)
        {
            using (var context = new ShopContext())
            {
                ShopServices shopServices = new ShopServices();
                ShoppingCart shoppingCart = new ShoppingCart();
                if (!context.Users.Any(u => u.Login == user.Login))
                {
                    user.Login = user.Login.ToLower();
                    user.ShoppingCartId = shoppingCart.Id;
                    shoppingCart.UserId = user.Id;
                    context.ShoppingCarts.Add(shoppingCart);
                    context.Users.Add(user);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("Login", "User Exists");
                ViewBag.RolesInput = new SelectList(new List<string>
                {
                "admin", "user", "manager"
                });
                return View(user);

            }
        }

        public ActionResult CreateProduct()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateProduct(Product product)
        {
            using (var context = new ShopContext())
            {
                context.Products.Add(product);
                context.SaveChanges();
                return RedirectToAction("ProductsTable");
            }
        }


        public ActionResult Index()
        {
            using (var context = new ShopContext())
            {
                context.SaveChanges();
                var products = context.Products.ToList();
                return View(products);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}