﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace asp.netSiteFw.Models
{
    public class User : GuidGenerator
    {
        [Required(ErrorMessage = "Login Needed")]
        [MinLength(3, ErrorMessage = "Minimum length 3 characters")]
        [MaxLength(30, ErrorMessage = "Maximum length 30 characters")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Password Needed")]
        [MaxLength(30, ErrorMessage = "Maximum 30 characters")]
        [RegularExpression(@"^(?=.*\d{2})(?=.*[a-zA-Z]{2}).{8,}$", ErrorMessage = "Needed minimum 2 digits, minimum length 8 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords did not match")]
        [DataType(DataType.Password)]
        [NotMapped]
        public string PasswordConfirm { get; set; }
        public string Role { get; set; } = "user";

        [Key, ForeignKey("ShoppingCart")]
        public Guid ShoppingCartId { get; set; }
        public virtual ShoppingCart ShoppingCart { get; set; }
    }
}
