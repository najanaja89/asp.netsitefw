﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.netSiteFw.Models
{
    public class Category : GuidGenerator
    {
        public Guid ShoppingCartId { get; set; }
        public virtual ShoppingCart ShoppingCart { get; set; }
    }
}