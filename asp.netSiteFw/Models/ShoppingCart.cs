﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace asp.netSiteFw.Models
{
    public class ShoppingCart : GuidGenerator
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        //public Guid ProductId { get; set; }
        //public ICollection<Product> Products;
        public virtual ICollection<Position> Positions { get; set; }
        public virtual User User { get; set; }
        public Position Position { get; set; } 

    }
}
