﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp.netSiteFw.Models
{
    public class Store : GuidGenerator
    {
        public Guid PositionId { get; set; }
        public Guid ProductId { get; set; }

        public Position Position { get; set; }
        //public Product Product { get; set; }
    }
}
