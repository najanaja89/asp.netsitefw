﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace asp.netSiteFw.Models
{
    public class Position : GuidGenerator
    {
        public Guid ShoppingCartId { get; set; }
        public virtual List<Store> Store { get; set; } = new List<Store>();
        public virtual ShoppingCart ShoppingCart { get; set; }
    }
}