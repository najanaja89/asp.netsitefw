﻿using asp.netSiteFw.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp.netSiteFw.DataContext
{
    public class DataInitialization : DropCreateDatabaseIfModelChanges<ShopContext>
    {
        //public override void InitializeDatabase(ShopContext context)
        //      {
        //          context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
        //              , string.Format("ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

        //          base.InitializeDatabase(context);
        //      }

        protected override void Seed(ShopContext context)
        {
            ShoppingCart shoppingCart = new ShoppingCart();

            User user = new User
            {
                Login = "najanaja",
                Password = "Root1234",
                Role = "admin",
                ShoppingCartId = shoppingCart.Id
            };

            shoppingCart.UserId = user.Id;
            context.Products.AddRange(new List<Product>
            {
               new Product { Name= "asian", Price = 12000},
               new Product { Name= "european", Price = 20000},
               new Product { Name= "russian", Price = 30000},
               new Product { Name= "american", Price = 2500},
            });

            context.ShoppingCarts.Add(shoppingCart);
            context.Users.Add(user);
            context.SaveChanges();
        }

    }
}
