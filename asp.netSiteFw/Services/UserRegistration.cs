﻿using asp.netSiteFw.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace asp.netSiteFw.Services
{
    public class UserRegistration
    {

        User User { get; set; }
        ShoppingCart ShoppingCart { get; set; }

        public UserRegistration()
        {
            ShoppingCart shoppingCart = new ShoppingCart();

            User user = new User
            {
                Login = "",
                Password = "",
                ShoppingCartId = shoppingCart.Id
            };

            shoppingCart.UserId = user.Id;

            User = user;
            ShoppingCart = shoppingCart;
        }


        public void ValidationCheck()
        {

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            //int lengthPassword = User.Password.Length;
            //int minimalLengthOfPassword = 8;

            Console.WriteLine("Enter Login name: ");
            User.Login = Console.ReadLine().ToLower();
            //User.ShoppingCartId = ShoppingCart.Id;

            while (true)
            {
                while (true)
                {
                    Console.WriteLine("Enter Password: ");
                    User.Password = ReadPassword();
                    var isValidated = hasNumber.IsMatch(User.Password) && hasUpperChar.IsMatch(User.Password) && hasMinimum8Chars.IsMatch(User.Password);
                    if (isValidated)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Password must contain minimum 8 symbols, one upper case letter, one digit");
                    }
                }

                Console.WriteLine("Retype Password: ");
                string doublePassword = ReadPassword();
                if (doublePassword != User.Password)
                {
                    Console.WriteLine("Passwords did not match!");
                }
                else break;
            }

        }

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {

                        password = password.Substring(0, password.Length - 1);
                        int pos = Console.CursorLeft;
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        Console.Write(" ");
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            Console.WriteLine();
            return password;
        }

        public User ExportUser()
        {
            return User;
        }

        public ShoppingCart ExportShoppingCart()
        {
            return ShoppingCart;
        }

    }

}

