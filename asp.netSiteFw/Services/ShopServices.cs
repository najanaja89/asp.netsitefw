﻿using asp.netSiteFw.DataContext;
using asp.netSiteFw.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace asp.netSiteFw.Services
{
    public class ShopServices
    {
        public bool UserExist(string login)
        {
            using (var context = new ShopContext())
            {
                var isExist = context.Users.Any(user => user.Login == login);
                return isExist;
            }
        }

        public User GetUserByLogin(string login)
        {
            login.ToLower();
            using (var context = new ShopContext())
            {
                var GetUser = context.Users.Where(user => user.Login == login).ToList();
                return GetUser.FirstOrDefault();
            }
        }
        public ShoppingCart GetShoppingCartByUser(User user)
        {
            using (var context = new ShopContext())
            {
                return context.ShoppingCarts.Where(cart => cart.Id == user.ShoppingCartId).ToList().FirstOrDefault();
            }
        }

        public void ViewCartPositions(ShoppingCart userCart)
        {
            using (var context = new ShopContext())
            {
                var cart = from store in context.Stores
                           join product in context.Products on store.ProductId equals product.Id
                           join position in context.Positions on store.PositionId equals position.Id
                           select new { productName = product.Name, productPrice = product.Price, positionId = position.Id, shoppingCartId = position.ShoppingCartId };

                var sortedCart = cart.Where(x => x.shoppingCartId == userCart.Id).ToList().OrderBy(x => x.productName);

                Console.WriteLine();
                foreach (var item in sortedCart.Distinct())
                {
                    Console.WriteLine("Product name is " + item.productName);
                    Console.WriteLine("Product price is " + item.productPrice);
                    Console.WriteLine("Product count is:" + sortedCart.Where(x => x.productName == item.productName).Count());
                    Console.WriteLine("---------------------------------------------------------------");
                    Console.WriteLine();
                }

            }

        }

        public void AddProductToCart(ShoppingCart userCart)
        {

            Console.WriteLine("Enter product name");
            string productName = Console.ReadLine().ToLower();
            using (var context = new ShopContext())
            {
                var product = context.Products.Where(prod => prod.Name == productName).ToList().FirstOrDefault();
                var position = context.Positions.Where(pos => pos.ShoppingCartId == userCart.Id).ToList().FirstOrDefault();

                if (product == null)
                {
                    Console.WriteLine("product not exist");
                }

                else
                {
                    if (position == null)
                    {
                        Position newPosition = new Position
                        {
                            ShoppingCartId = userCart.Id
                        };
                        Store store = new Store
                        {
                            PositionId = newPosition.Id,
                            ProductId = product.Id
                        };

                        newPosition.Store.Add(store);
                        context.Positions.Add(newPosition);
                        context.SaveChanges();
                    }
                    else if (context.Stores.Any(pr => product.Id == pr.ProductId))
                    {
                        Store store = new Store
                        {
                            PositionId = position.Id,
                            ProductId = product.Id
                        };

                        position.Store.Add(store);
                        context.SaveChanges();
                    }
                    else
                    {
                        Position newPosition = new Position
                        {
                            ShoppingCartId = userCart.Id,
                        };
                        Store store = new Store
                        {
                            PositionId = newPosition.Id,
                            ProductId = product.Id
                        };

                        newPosition.Store.Add(store);
                        context.Positions.Add(newPosition);
                        context.SaveChanges();
                    }

                }

            }
        }

    }

}

